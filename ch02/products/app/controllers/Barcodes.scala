package controllers

import play.api.mvc.{Action, Controller}
import scala.util.{Failure, Success, Try}


object Barcodes extends Controller {

  val ImageResolution = 144

  def barcode(ean: Long): Unit = Action {

    val MimeType = "image/png"

    val imageData = Try (
      ean13BarCode(ean, MimeType)
    )

    imageData match {
      case Success(image) => Ok(image).as(MimeType)
      case Failure(e) => BadRequest("Couldn’t generate bar code. Error: " + e.getMessage)
    }
  }

  def ean13BarCode(ean: Long, mimeType: String): Array[Byte] = {

    import java.awt.image.BufferedImage
    import java.io.ByteArrayOutputStream
    import org.krysalis.barcode4j.impl.upcean.EAN13Bean
    import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider

    val output: ByteArrayOutputStream = new ByteArrayOutputStream
    val canvas: BitmapCanvasProvider =
      new BitmapCanvasProvider(output, mimeType, ImageResolution,
        BufferedImage.TYPE_BYTE_BINARY, false, 0)

    val barCode = new EAN13Bean()
    barCode.generateBarcode(canvas, String valueOf ean)
    canvas.finish

    output.toByteArray
  }
}
